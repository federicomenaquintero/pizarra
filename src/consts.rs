/// Thikness in pixels used by default while drawing
pub const DEFAULT_THICKNESS: f64 = 3.0;

/// Padding in pixels added to drawings while exporting.
pub const EXPORT_PADDING: f64 = 20.0;

/// if two points are within this radius pizarra considers them to be touching
/// each other
pub const TOUCH_RADIUS: f64 = 10.0;
